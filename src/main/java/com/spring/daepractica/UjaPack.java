/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica;

import com.spring.daepractica.clases.Oficina;
import com.spring.daepractica.clases.CentroLogistico;
import com.spring.daepractica.clases.Paquete;
import com.spring.daepractica.clases.Remitente;
import java.util.ArrayList;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Pacalor
 */
@SpringBootApplication(scanBasePackages = "com.spring.daepractica.beans")
public class UjaPack {
    public static void main(String[] args) throws Exception {
        SpringApplication servidor = new SpringApplication(UjaPack.class);
        /* Eliminar opcionalmente el banner de SpringBoot */
        // server.setBannerMode(Banner.Mode.OFF);
        ApplicationContext context=servidor.run(args);
    }
}
