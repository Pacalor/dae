/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.beans;


import com.spring.daepractica.clases.CentroLogistico;
import com.spring.daepractica.clases.Constantes;
import static com.spring.daepractica.clases.Constantes.EstadoPaquete.ENTREGADO;
import static com.spring.daepractica.clases.Constantes.EstadoPaquete.EN_REPARTO;
import com.spring.daepractica.clases.Paquete;
import static com.spring.daepractica.clases.Constantes.EstadoPaquete.EN_TRANSITO;
import static com.spring.daepractica.clases.Constantes.TIEMPO_BASE_SIMULACION;
import static com.spring.daepractica.clases.Constantes.VARIACION_TIEMPO_SIMULACION;
import static com.spring.daepractica.clases.Constantes.generador;
import com.spring.daepractica.clases.Oficina;
import com.spring.daepractica.clases.PuntoControl;
import com.spring.daepractica.clases.Remitente;
import com.spring.daepractica.datos.Nodo;
import com.spring.daepractica.excepciones.ProvinciaNoEncontrada;
import com.spring.daepractica.excepciones.ErrorLecturaJSON;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pacalor
 */
@Service
public class UjaPackService {
    private Map<Long, Paquete> paquetes;///< Mapa de los paquetes existentes
    private Map<String, Oficina> oficinas;///< Mapa de las oficinas 
    private Map<Integer, CentroLogistico> centrosLogisticos;///< mapa de los centros logisticos
    private Set<Long> localizadores;///< conjunto de long para almacenar localizadores unicos
    public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName("UTF-8");
    
    public UjaPackService(){
        paquetes = new HashMap<>();
        oficinas =  new HashMap<>();
        centrosLogisticos =  new HashMap<>();
        localizadores = new HashSet<>();
        try{
            cargaDatos();
        }catch(Exception e){
            System.out.println("--ERROR EN LA LECTURA DEL JSON--");
            throw new ErrorLecturaJSON();
        }
        
        //prueba();
    }
    /**
     * @brief funcion para confirmar un paquete
     * @param paquete paquete que se confirma para su envio 
     * @return bool para indicar que se ha confirmado correctamente
     */
    public boolean confirmarPaquete(Paquete paquete){
        if(!paquete.getConfirmado()){
            paquete.setConfirmado(true);
        }        
        return true;
    }
    /**
     * @brief funcion para crear un nuevo paquete dados unos parametros
     * @param peso peso del paquete en kg
     * @param dimension dimensiones del paquete en cm cubicos
     * @param destino lugar de destino del paquete
     * @param remitente persona que envia el paquete
     * @return el objeto paquete que hemos creado
     */
    public Paquete crearNuevoPaquete(double peso, double dimension, String destino, Remitente remitente) throws Exception{
        Paquete paquete = new Paquete(peso, dimension, destino, remitente);    
        long localizador = generarLocalizador();  
        paquetes.put(localizador, paquete);
        return paquete;
    }
    /**
     * @brief funcion auxiliar para calcular un localizador de 10 cifras unico
     * @return localizador unico de 10 cifras unico de un paquete
     */
    private long generarLocalizador(){
        long minimoLocalizador = 1000000000L, maximaVariacionLocalizador = 9000000000L;
        long localizador = minimoLocalizador + (generador.nextLong() % maximaVariacionLocalizador);
        
        while(localizadores.contains(localizador)){
            localizador = minimoLocalizador + (generador.nextLong() % maximaVariacionLocalizador);
        }
        localizadores.add(localizador);
        return localizador;
    }
    /**
     * @brief funcion para actualizar el punto de control de un paquete
     * @param paquete paquete del cual necesitamos actualizar su punto de control
     * @return true si es el final de la ruta(no hay mas puntos de control, false en caso contrario
     */
    public boolean actualizarPControl(Paquete paquete) throws Exception{
        LocalDate fechaActual = LocalDate.now();
        ArrayList<PuntoControl> ruta = paquete.getRuta();
        
        int situacion = paquete.getSituacion();
        ruta.get(situacion).setFechaSalida(fechaActual);
        situacion++;
        
        if(situacion < ruta.size()){
            paquete.setSituacion(situacion);
            //int tiempoSimulacion = generador.nextInt(VARIACION_TIEMPO_SIMULACION) + TIEMPO_BASE_SIMULACION;
            //TimeUnit.SECONDS.sleep(tiempoSimulacion);
            fechaActual = LocalDate.now();
            ruta.get(situacion).setFechaLlegada(fechaActual);//---->sumar tiempo de simulacion!!<-----
            
            return false;
        }else{
            return true;
        }
        
    }
    /**
     * @brief funcion para actualizar punto de control y estado de los paquetes existentes
     * @return true si se ha llevado a cabo correctamente, falso en caso contrario
     * @throws Exception 
     */
    public boolean siguientePaso() throws Exception{
        Iterator<Paquete> iteradorPaquetes = paquetes.values().iterator();
        while (iteradorPaquetes.hasNext()){
            Paquete paqueteActual = iteradorPaquetes.next();
            boolean finRuta = true;
            
            if(paqueteActual.getEstado() != ENTREGADO){//Si estaba entregado no hace nada
                if(paqueteActual.getEstado() == EN_REPARTO){//Si estaba en reparto pasa a entregado
                    paqueteActual.setEstado(ENTREGADO);
                }else{
                    //Si esta en transito, actualizamos el punto de control
                    if(paqueteActual.getEstado() == EN_TRANSITO){
                        finRuta = actualizarPControl(paqueteActual);
                    }
                    if(finRuta){//Si llegamos al final de ruta, lo ponemos en reparto
                        paqueteActual.setEstado(EN_REPARTO);
                    }
                }
            }
        }
        return true;
    }
    /**
     * @brief funcion para dar informacion de un paquete a partir de su localizador
     * @param localizador clave para localizar el paquete en el mapa
     * @return paquete del que damos la informacion
     */
    public Paquete darInfoPaquete(long localizador){
        Paquete paquete = paquetes.get(localizador);
        
        
        
        return paquete;
    }
    
    /**
     * @brief función de carga de datos desde JSON, creando oficinas y centros Logísticos
     */
    private void cargaDatos(){
        
        Oficina nuevaOficina;
        CentroLogistico nuevoCentro;
        ArrayList<Integer> nuevasConexiones;
        ArrayList<Oficina> nuevasOficinas;
        String nombre;
        String provincia;
        
        JSONParser jsonParser = new JSONParser();
        Path path = Paths.get("src/main/java/com/spring/daepractica/datos/redujapack.json");
        int idOficina=0;
        
         try(FileReader reader = new FileReader(path.toString()))
        {
            //creación del buffer de lectura
            BufferedReader b= new BufferedReader(reader);
            b= new BufferedReader(new InputStreamReader(
            new FileInputStream(new File(path.toString())), UTF_8));
            
            //pasar todo el archivo a un solo string de UTF_8
            String texto="";
            String linea;
            while((linea=b.readLine())!=null){
                texto=texto.concat(linea);
            }            
            
            //Leer JSON
            Object obj = jsonParser.parse(texto);
            JSONObject json = (JSONObject) obj;
            
            
            int n=json.size();
            
            //bucle de creación y parseo de datos de JSON a las clases Oficicina y CentroLogistic  
            for (int i = 1; i <= n; i++) {
                
                nuevasOficinas = new ArrayList<>();
                nuevasConexiones = new ArrayList<>();
                
                //System.out.println(json.get(String.valueOf(i)));
                JSONObject datos = (JSONObject) json.get(String.valueOf(i));
                
                                
                //carga de las provincias
                JSONArray provincias = (JSONArray) datos.get("provincias");
                
                for (int j = 0; j < provincias.size(); j++) {//creamos todas las oficinas
                    nuevaOficina=new Oficina(idOficina, provincias.get(j).toString());
                    
                    nuevasOficinas.add(nuevaOficina);//oficinas 
                    oficinas.put(provincias.get(j).toString(), nuevaOficina);
                    idOficina++;
                    //System.out.println(provincias.get(j).toString().contains("Granada"));
                    //System.out.println(nuevaOficina.getProvincia());
                }
               
                
                //carga de las conexiones
                JSONArray conexiones = (JSONArray) datos.get("conexiones");
                
                for (int j = 0; j < conexiones.size(); j++) {
                    nuevasConexiones.add(Integer.parseInt(conexiones.get(j).toString()));  
                }
                //System.out.println(nuevasConexiones);
                
                
                //carga del nombre
                nombre = (String) datos.get("nombre");
                //System.out.println(nombre);
                
                //carga de la provincia
                provincia = (String) datos.get("localización");
                //System.out.println(provincia);
                
                
                nuevoCentro = new CentroLogistico(i, nombre, provincia, nuevasConexiones, nuevasOficinas);
                centrosLogisticos.put(i, nuevoCentro);
            }
            //System.out.println(centrosLogisticos.size());
 
        } catch (Exception e) {
            throw new ErrorLecturaJSON();
        }
         
    }
    
    /**
     * @brief función que calcula la ruta del paquete
     * @param origen lugar de salida del paquete
     * @param p paquete que va a ser calculada su ruta
     * @return  paquete modificado
     */
    private Paquete calculaRuta(String origen, Paquete p){
        String destino=p.getDestino();
        int origenID=0;
        int destinoID=0;
        ArrayList<PuntoControl> ruta=new ArrayList<>();
        ArrayList<Integer> camino=new ArrayList<>();
        Oficina oficinaTemp;
        CentroLogistico centroTemp;
        
        //buscar el id del centro logistico de el origen y el destino
        for (Map.Entry<Integer, CentroLogistico> entry : centrosLogisticos.entrySet()) {
            if(entry.getValue().contieneProvincia(destino))
                destinoID=entry.getValue().getIdentificador();
            if(entry.getValue().contieneProvincia(origen))
                origenID=entry.getValue().getIdentificador();    
        }
        
        if(destinoID == 0 || origenID ==0)//en caso de haber en el origen y/o destino
            throw new ProvinciaNoEncontrada();
        
        
        if(destinoID==origenID && destino.contains(origen)){//CASO A: ORIGEN Y DESTINO MISMA PROVINCIA
            //System.out.println("---MISMA PROVINCIA----");
            ruta.add(new PuntoControl(oficinas.get(origen)));
            p.setRuta(ruta);
            
            
        }else if(centrosLogisticos.get(origenID).contieneProvincia(destino)){//CASO B: ORIGEN Y DESTINO MISMO CENTRO LOGISTICO
            //System.out.println("---MISMA CENTRO LOGISTICO----");
            ruta.add(new PuntoControl(oficinas.get(origen)));
            ruta.add(new PuntoControl(centrosLogisticos.get(origenID)));
            //ruta.add(new PuntoControl(centrosLogisticos.get(destinoID)));
            ruta.add(new PuntoControl(oficinas.get(destino)));
            p.setRuta(ruta);
            
            
        }else{//CASO C: DISTINTO CENTRO LOGISTICO
            ruta.add(new PuntoControl(oficinas.get(origen)));
            camino=algoritmoCamino(origenID, destinoID);
            
            for (Integer integer : camino) {
                ruta.add(new PuntoControl(centrosLogisticos.get(integer)));
            }
            
            ruta.add(new PuntoControl(oficinas.get(destino)));
            p.setRuta(ruta);
        }
            return p;
    }
    
    
    /**
     * @brief el algoritmo crea un nodo, el primer nodo es el centro logístico de partida
     * pregunta si el nodo que tenemos es el destino, si es así devuelve el camino seguido para ese nodo
     * en caso de que no toma todas sus conexiones y crea nodos hijos, eliminandose a si mismo de la lista de nodos a comprobar
     * @param origen lugar de salida del paquete
     * @param destino destino del paquete
     * @return lista de enteros, identificadores de los centros logísticos
     */
    private ArrayList<Integer> algoritmoCamino(int origen, int destino){
        ArrayList<Integer> camino=new ArrayList<>();
        ArrayList<Nodo> nodos= new ArrayList<>();
        Set<Integer> prohibido = new HashSet<>();
        Nodo nodoActual;
        ArrayList<Integer> conexiones=new ArrayList<>();
        
        prohibido.add(origen);
        nodos.add(new Nodo(origen,camino));
        
        
        while (!nodos.isEmpty()) {
            nodoActual=nodos.remove(0);
            conexiones=centrosLogisticos.get(nodoActual.getId()).getConexiones();
            
            if(nodoActual.getId()==destino){
                return nodoActual.getCamino();
            }
            
            //System.out.println("algoritmo de busqueda sobre centro id "+centrosLogisticos.get(nodoActual.getId()).getIdentificador());
            //System.out.println(conexiones);
            
            for (Integer conexione : conexiones) {
                
                if(!prohibido.contains(conexione)){
                    
                    nodos.add(nodos.size(),new Nodo(conexione,nodoActual.getCamino()));
                    prohibido.add(conexione);
                    //System.out.println("numeros tabu"+prohibido);
                }
            }
            
        }
        
        return camino;
    }
    
    /**
     * @brief calcula el precio del paquete y devielve el paquete con el precio modificado
     * @param origen lugar de salida del paquete
     * @param p paquete que va a ser calculado su precio
     * @return  paquete modificado
     */
    public Paquete calcularPrecio(String origen,Paquete p){
        Paquete temporal;
        temporal = calculaRuta(origen,p);
        temporal.setPrecio(
                (temporal.getPeso() * temporal.getDimension() * ((double)temporal.getRuta().size()+1.0))/1000
        );
        return temporal;
    }
    
    /**
     * funcion de prueba con salidas en consola para testear la aplicación, anterior a los test
     */
    public void prueba(){
        System.out.println("INICIO DE LA PRUEBA");
        Paquete p=null;
        try {
            p = new Paquete(1.0,5.0,"Granada",new Remitente("juan","el pruebas",123456789,"vive aqui"));
        } catch (Exception ex) {
            Logger.getLogger(UjaPackService.class.getName()).log(Level.SEVERE, null, ex);
        }
        p=calcularPrecio("Valencia", p);
        
        
        
        ArrayList<PuntoControl> prueba= p.getRuta();
        
       
        for (PuntoControl puntoControl : prueba) {
            if(puntoControl.esCentro())
                System.out.println(puntoControl.getCentro().getNombre());
            else
                System.out.println(puntoControl.getOficina().getProvincia());
        }
        
        p.setPrecio(
                (p.getPeso() * p.getDimension() * ((double)p.getRuta().size()+1.0))/1000
        );
        System.out.println(p.getPrecio());
    }
}
