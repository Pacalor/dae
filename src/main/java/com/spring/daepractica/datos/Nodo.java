/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.datos;

import java.util.ArrayList;

/**
 *
 * @author Pacalor
 */
public class Nodo {
    private int id;
    private ArrayList<Integer> camino;

    public Nodo(int id, ArrayList<Integer> camino) {
        this.id = id;
        this.camino = new ArrayList<Integer>(camino);
        this.camino.add(id);
    }

    public int getId() {
        return id;
    }

    public ArrayList<Integer> getCamino() {
        return camino;
    }
    
}
