/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import java.time.LocalDate;

/**
 *
 * @author migue
 */
public class Oficina {
    private int id;///< Identificador de la oficina
    private String provincia;///< Provincia donde se localiza la oficina

    public Oficina(int id, String provincia) {
        this.id = id;
        this.provincia = provincia;
    }
    
    /**
     * @brief Establece al Punto de Control la fecha de salida actual
     * @param pControl 
     */
    public void fechaControlSalida(PuntoControl pControl){
        pControl.setFechaSalida(LocalDate.now());
    }
    /**
     * @brief Establece al Punto de Control la fecha de entrada actual
     * @param pControl 
     */
    public void fechaControlEntrada(PuntoControl pControl){
        pControl.setFechaSalida(LocalDate.now());
    }
    
    /**
     * 
     * @return Identificador de la oficina
     */
    public int getId(){
        return this.id;
    }

    public String getProvincia() {
        return provincia;
    }
    
     
}
