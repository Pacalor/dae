/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import java.util.Random;

/**
 *
 * @author migue
 */
public class Constantes {
    //Generador de numeros aleatorio
    public static final Random generador = new Random();
    
    // Tipo enum para el Estado de un paquete
    public enum EstadoPaquete{
        EN_REPARTO,///< Estado del paquete cuando esta en una oficina o centro logistico
        EN_TRANSITO,///< Estado del paquete cuando se encuentra en el vehiculo de reparto
        ENTREGADO;///< Estado del paquete cuando ya ha sido entregado por el vehiculo de reparto
    }
            
    //Constantes
    public static final int TIEMPO_BASE_SIMULACION = 3;
    public static final int VARIACION_TIEMPO_SIMULACION = 5;

}
