/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import java.time.LocalDate;
import java.util.ArrayList;


/**
 *
 * @author migue
 */
public class CentroLogistico {
    private int identificador;///< Identificador del Centro Logistico
    private String nombre;///< Nombre del Centro Logistico
    private String provincia;///< Provincia del Centro Logistico
    private ArrayList<Integer> conexiones;///< Ids de los Centros Logisticos con los que tiene conexion
    private ArrayList<Oficina> oficinas;///Oficinas con conexion a este Centro Logistico
    
    public CentroLogistico(int identificador, String nombre, String provincia, ArrayList<Integer> conexiones, ArrayList<Oficina> oficinas) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.provincia = provincia;
        this.conexiones = conexiones;
        this.oficinas = oficinas;
    }

    public int getIdentificador() {
        return identificador;
    }

    public String getProvincia() {
        return provincia;
    }

    public ArrayList<Integer> getConexiones() {
        return conexiones;
    }
    /**
     * @brief Establece al Punto de Control la fecha de salida actual
     * @param pControl 
     */
    public void fechaControlSalida(PuntoControl pControl){
        pControl.setFechaSalida(LocalDate.now());
    }
    /**
     * @brief Establece al Punto de Control la fecha de entrada actual
     * @param pControl 
     */
    public void fechaControlEntrada(PuntoControl pControl){
        pControl.setFechaSalida(LocalDate.now());
    }
    
    public boolean contieneProvincia(String provincia){
        for (Oficina oficina : oficinas) {
            if(oficina.getProvincia().equals(provincia))
                return true;
        }
        return false;
    }

    public String getNombre() {
        return nombre;
    }
    
}
