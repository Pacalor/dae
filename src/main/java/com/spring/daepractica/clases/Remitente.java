/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

/**
 *
 * @author migue
 */
public class Remitente {
    private String nombre;///< Nombre del remitente del paquete
    private String apellidos;///< Apellidos del remitente del paquete
    private int telefono;///< Telefono del remitente del paquete
    private String direccion;///< Direccion del remitente del paquete

    public Remitente(String nombre, String apellidos, int telefono, String direccion) throws Exception {
        if(!telefonoCorrecto(((Integer)telefono))){
            throw new Exception("El telefono debe ser de 9 cifras");
        }
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public int getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }
    
    /**
     * @brief funcion para comprobar si el telefono tiene 9 cifras
     * @return true si es de 9 cifras, false en caso contrario
     */
    public static boolean telefonoCorrecto(Integer telefono) {
        if(telefono.toString().length() != 9){
            return false;
        }
        return true;
    }
}
