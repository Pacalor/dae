/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author migue
 */
public class PuntoControl {
    private LocalDate fechaLlegada;///< Fecha de llegada al punto de control
    private LocalDate fechaSalida;///< Fecha de salida del punto de control
    private Oficina oficina;///< Oficina del punto de control
    private CentroLogistico centro;/// Centro Logistico del punto de control

    public PuntoControl(Oficina oficina) {
        this.oficina = oficina;
        this.centro = null;
    }

    public PuntoControl(CentroLogistico centro) {
        this.centro = centro;
        this.oficina = null;
    }

    
    public LocalDate getFechaLlegada() {
        return fechaLlegada;
    }

    public void setFechaLlegada(LocalDate fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    public LocalDate getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(LocalDate fechaSalida) {
        this.fechaSalida = fechaSalida;
    }
    
    public boolean esCentro(){
        if(centro==null) return false;
        else
         return true;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public CentroLogistico getCentro() {
        return centro;
    }
    
    
}
