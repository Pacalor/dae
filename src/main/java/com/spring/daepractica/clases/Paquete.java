/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import com.spring.daepractica.clases.Constantes.EstadoPaquete;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author migue
 */
public class Paquete {
    private final double peso;///< Peso del paquete en Kg
    private final double dimension;///< Dimension del paquete en Cm cúbicos(Cm^3)
    private double precio;///< Precio del paquete en Euros(€)
    private long localizador;///< Codigo localizador del paquete
    private final String destino;///< Lugar de destino del paquete
    private final Remitente remitente;///< Remitente del paquete
    private int situacion;///< Posicion en el vector de Puntos de Control
    private String dniEntrega;///< DNI del receptor del paquete
    private boolean confirmado;///< Estado de confirmacion del paquete
    private LocalDate fechaEntrega;///< Fecha de entrega del paquete
    private EstadoPaquete estado;///< Estado del paquete(EN_REPARTO, EN_TRANSITO, ENTREGADO)
    private ArrayList<PuntoControl> ruta;///< Ruta de puntos de control del paquete

    public Paquete(double peso, double dimension, String destino, Remitente remitente) throws Exception {
        if(!comprobarPeso(peso)){
            throw new Exception("El peso debe ser positivo");
        }
        if(!comprobarDimension(dimension)){
            throw new Exception("La dimension debe ser positiva");
        }
        this.peso = peso;
        this.dimension = dimension;
        this.localizador = 0;
        this.destino = destino;
        this.confirmado = false;
        this.estado = EstadoPaquete.EN_TRANSITO;
        this.remitente = remitente;
        this.situacion = 0;
        this.precio = 0.0 ;
        this.dniEntrega = "";
        this.ruta = new ArrayList<>();
    }

    public long getLocalizador() {
        return localizador;
    }

    public int getSituacion() {
        return situacion;
    }

    public void setEstado(EstadoPaquete estado) {
        this.estado = estado;
    }

    public EstadoPaquete getEstado() {
        return estado;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setSituacion(int situacion) throws Exception {
        if(!comprobarSituacion(situacion)){
            throw new Exception("La situacion debe ser un entero positivo");                    
        }
        this.situacion = situacion;
    }

    public boolean getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(boolean confirmado) {
        this.confirmado = confirmado;
    }

    public ArrayList<PuntoControl> getRuta() {
        return ruta;
    }

    public void setRuta(ArrayList<PuntoControl> ruta) {
        this.ruta = ruta;
    }

    public String getDestino() {
        return destino;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDniEntrega() {
        return dniEntrega;
    }

    public void setDniEntrega(String dniEntrega) throws Exception {
        if(!comprobarDniEntrega(dniEntrega)){
            throw new Exception("El dni debe ser de 8 numeros y una letra");
        }
        this.dniEntrega = dniEntrega;
    }

    public double getPeso() {
        return peso;
    }

    public double getDimension() {
        return dimension;
    }
    /**
     * @brief funcion para comprobar que el peso sea correcto
     * @param peso el valor a comprobar
     * @return true si es correcto, false si no
     */
    public static boolean comprobarPeso(double peso) {
        if(peso < 0){
            return false;
        }
        return true;
    }
    /**
     * @brief funcion para comprobar que la dimension sea correcta
     * @param dimension el valor a comprobar
     * @return true si es correcta, false si no
     */
    public static boolean comprobarDimension(double dimension) {
        if(dimension < 0){
            return false;
        }
        return true;    
    }
    /**
     * @brief funcion para comprobar que el dniEntrega sea correcto
     * @param dniEntrega el valor a comprobar
     * @return true si es correcta, false si no
     */
    public static boolean comprobarDniEntrega(String dniEntrega) {
        //char letras[] = {'A', 'B', 'C', 'D', 'E', 'F','G', 'H','I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        if(dniEntrega.matches("[0-9]{8}[A-Z]{1}")){
            return true;
        }
        return false;
    }  
    /**
     * @brief funcion para comprobar que la situacion sea correcta
     * @param situacion el valor a comprobar
     * @return true si es correcta, false si no
     */
    public static boolean comprobarSituacion(int situacion) {
        if(situacion < 0){
            return false;
        }
        return true;    
    }

    public Remitente getRemitente() {
        return remitente;
    }

    public void setLocalizador(long localizador) {
        this.localizador = localizador;
    }
    
    
}
