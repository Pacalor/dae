/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.beans;

import com.spring.daepractica.UjaPack;
import com.spring.daepractica.clases.Constantes;
import static com.spring.daepractica.clases.Constantes.EstadoPaquete.ENTREGADO;
import static com.spring.daepractica.clases.Constantes.EstadoPaquete.EN_REPARTO;
import static com.spring.daepractica.clases.Constantes.EstadoPaquete.EN_TRANSITO;
import com.spring.daepractica.clases.Paquete;
import com.spring.daepractica.clases.PuntoControl;
import com.spring.daepractica.clases.Remitente;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 *
 * @author Pacalor
 */

@SpringBootTest(classes = {UjaPack.class})
public class UjaPackServiceTest {
    
    @Autowired 
    UjaPackService uja;
    
    /**
     * Test of confirmarPaquete method, of class UjaPackService.
     */
    @Test
    public void testConfirmarPaquete() throws Exception {
        Paquete paquete1 = null;
        Paquete paquete2 = null;

        try {
            paquete1 = new Paquete(1.0, 5.0, "Granada", new Remitente("juan", "el pruebas", 123456789, "vive aqui"));
            paquete2 = new Paquete(2.0, 3.0, "Sevilla", new Remitente("maria", "la pruebas", 123411789, "desconocido"));
        } catch (Exception ex) {
            Assertions.assertTrue(false);
        }
        
        uja.confirmarPaquete(paquete1);
        
        Assertions.assertTrue(paquete1.getConfirmado());
        Assertions.assertFalse(paquete2.getConfirmado());
    }

    /**
     * Test of crearNuevoPaquete method, of class UjaPackService.
     */
    @Test
    public void testCrearNuevoPaquete() throws Exception {
        double peso = 5, dimension = 240;
        String destino = "Sevilla";
        Remitente remitente = new Remitente("juan", "el pruebas", 123456789, "vive aqui");
        
        Paquete paquete1 = uja.crearNuevoPaquete(peso, dimension, destino, remitente);
        
        Assertions.assertTrue(paquete1.getPeso() == peso);
        Assertions.assertTrue(paquete1.getDimension() == dimension);
        Assertions.assertTrue(paquete1.getRemitente() == remitente);
        
        Remitente remitente2 = new Remitente("maria", "la pruebas", 123411789, "desconocido");
        
        Assertions.assertFalse(paquete1.getPeso() == peso + 1);
        Assertions.assertFalse(paquete1.getDimension() == dimension + 1);
        Assertions.assertFalse(paquete1.getDestino().equals(destino.concat(" betis")));
        Assertions.assertFalse(paquete1.getRemitente() == remitente2);
    }

    /**
     * Test of actualizarPControl method, of class UjaPackService.
     */
    @Test
    public void testActualizarPControl() throws Exception {
        //Creamos un paquete
        Paquete paquete1 = null;

        try {
            paquete1 = new Paquete(1.0, 5.0, "Granada", new Remitente("juan", "el pruebas", 123456789, "vive aqui"));
        } catch (Exception ex) {
            Assertions.assertTrue(false);
        }
        //Obtenemos la situacion del paquete, que inicialmente será 0
        int situacion = paquete1.getSituacion();
        //Calculamos el precio y ruta
        uja.calcularPrecio("Burgos", paquete1);
        //Obtenemos la ruta del paquete
        ArrayList<PuntoControl> ruta = paquete1.getRuta();
        //Actualizamos el punto de control de la ruta
        uja.actualizarPControl(paquete1);
        //Obtenemos la fecha de salida del PC inicial y la de llegada del PC siguiente
        LocalDate fechaSalidaPuntoActual = ruta.get(situacion).getFechaSalida();
        LocalDate fechaLlegadaPuntoSiguiente = ruta.get(++situacion).getFechaLlegada();
        
        Assertions.assertTrue((situacion) == paquete1.getSituacion());
        Assertions.assertTrue(ruta.get(situacion) == ruta.get(paquete1.getSituacion()));
        //Assertions.assertTrue(fechaSalidaPuntoActual.isBefore(fechaLlegadaPuntoSiguiente));
        
    }

    /**
     * Test of siguientePaso method, of class UjaPackService.
     */
    @Test
    public void testSiguientePaso() throws Exception {
        Paquete paquete1 = uja.crearNuevoPaquete(1.0,5.0,"Granada",new Remitente("juan","el pruebas",123456789,"vive aqui"));
        Paquete paquete2 = uja.crearNuevoPaquete(1.0,5.0,"Granada",new Remitente("maria","la pruebas",129996789,"nose aqui"));
        Paquete paquete3 = uja.crearNuevoPaquete(1.0,5.0,"Granada",new Remitente("jose","el pepe",123456000,"vive xd"));
        
        uja.calcularPrecio("Burgos", paquete1);
        uja.calcularPrecio("Murcia", paquete2);
        uja.calcularPrecio("Granada", paquete3);
        
        Assertions.assertTrue(paquete1.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete2.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete3.getEstado() == EN_TRANSITO);
        
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete2.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete3.getEstado() == EN_REPARTO);
        
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete2.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete3.getEstado() == ENTREGADO);
        
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete2.getEstado() == EN_TRANSITO);
        
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete2.getEstado() == EN_REPARTO);
        
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == EN_TRANSITO);
        Assertions.assertTrue(paquete2.getEstado() == ENTREGADO);
        
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == EN_REPARTO);
       
        uja.siguientePaso();
        
        Assertions.assertTrue(paquete1.getEstado() == ENTREGADO); 
    }

    /**
     * Test of calcularPrecio method, of class UjaPackService.
     */
    @Test
    public void testCalcularPrecio() {
        Paquete p1=null;
        Paquete p2=null;
        Paquete p3=null;
        try {
            p1 = new Paquete(1.0,5.0,"Granada",new Remitente("juan","el pruebas",123456789,"vive aqui"));
            p2 = new Paquete(1.0,5.0,"Granada",new Remitente("juan","el pruebas",123456789,"vive aqui"));
            p3 = new Paquete(1.0,5.0,"Granada",new Remitente("juan","el pruebas",123456789,"vive aqui"));
        } catch (Exception ex) {
            Assertions.assertTrue(false);
        }
        
        //p=UjaPackService.calculaRuta("Las Palmas", p);
        double precio1= (1.0*5.0*2)/1000;
        double precio2= (1.0*5.0*4)/1000;
        double precio3= (1.0*5.0*5)/1000;
        p1=uja.calcularPrecio("Granada", p1);
        p2=uja.calcularPrecio("Jaén", p2);
        p3=uja.calcularPrecio("Valencia", p3);
        
        Assertions.assertTrue(precio1==p1.getPrecio());
        Assertions.assertTrue(precio2==p2.getPrecio());
        Assertions.assertTrue(precio3==p3.getPrecio());
    }

    /**
     * Test of CalcularRuta method, of class UjaPackService.
     */
    @Test
    public void tesRuta() {
        
        
    }
    
}
