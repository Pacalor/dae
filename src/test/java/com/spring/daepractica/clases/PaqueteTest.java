/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author migue
 */
public class PaqueteTest {
    
    @Test
    public void testPeso() {
        double peso1 = 20;
        double peso2 = 0;
        double peso3 = 25.4;
        double peso4 = -1;
        
        Assertions.assertTrue(Paquete.comprobarPeso(peso1));
        Assertions.assertTrue(Paquete.comprobarPeso(peso2));
        Assertions.assertTrue(Paquete.comprobarPeso(peso3));
        Assertions.assertFalse(Paquete.comprobarPeso(peso4));
    }    
    
    @Test
    public void testDimesion() {
        double dimension1 = 20;
        double dimension2 = 0;
        double dimension3 = 25.4;
        double dimension4 = -1;
        
        Assertions.assertTrue(Paquete.comprobarDimension(dimension1));
        Assertions.assertTrue(Paquete.comprobarDimension(dimension2));
        Assertions.assertTrue(Paquete.comprobarDimension(dimension3));
        Assertions.assertFalse(Paquete.comprobarDimension(dimension4));
    } 
    
    @Test
    public void testDniEntrega() {
        String dni1 = "77389232A";
        String dni2 = "1234567AB";
        String dni3 = "12345678";
        String dni4 = "123456789";
        String dni5 = "123456W";
        
        Assertions.assertTrue(Paquete.comprobarDniEntrega(dni1));
        Assertions.assertFalse(Paquete.comprobarDniEntrega(dni2));
        Assertions.assertFalse(Paquete.comprobarDniEntrega(dni3));
        Assertions.assertFalse(Paquete.comprobarDniEntrega(dni4));
        Assertions.assertFalse(Paquete.comprobarDniEntrega(dni5));
    } 
    
    @Test
    public void testSituacion() {
        int situacion1 = 20;
        int situacion2 = 0;
        int situacion3 = -1;
        
        Assertions.assertTrue(Paquete.comprobarDimension(situacion1));
        Assertions.assertTrue(Paquete.comprobarDimension(situacion2));
        Assertions.assertFalse(Paquete.comprobarDimension(situacion3));
    } 
    
}
