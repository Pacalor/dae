/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.daepractica.clases;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author migue
 */
public class RemitenteTest {
    
    @Test
    public void testLongitudTelefono() {
        int telefono1 = 123456789;
        int telefono2 = 12345678;
        int telefono3 = 1234567891;
        int telefono4 = 654321987;
        
        Assertions.assertTrue(Remitente.telefonoCorrecto(telefono1));
        Assertions.assertFalse(Remitente.telefonoCorrecto(telefono2));
        Assertions.assertFalse(Remitente.telefonoCorrecto(telefono3));
        Assertions.assertTrue(Remitente.telefonoCorrecto(telefono4));
    }    
}
